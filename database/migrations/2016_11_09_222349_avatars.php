<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Avatars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avatars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('email_hash');
            $table->enum('mime_type', ["image/jpeg", "image/png", "image/gif", "image/bmp"]);
            $table->string('code_delete')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unique('email');
            $table->unique('email_hash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
