# Proyecto Avatar prueba Intraway

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Proyecto Avatar prueba Intraway

Este proyecto esta construido con el framework Lravel 5.3

Frameworks y librerias
- Laravel 5.3
- Intervetion Image https://github.com/Intervention/image
- Monolog https://github.com/Seldaek/monolog
- Mailgun Driver

Requisitos

- PHP >=5.6
- GD Library (>=2.0) (No soporta BMP) o Imagick PHP extension (>=6.5.7)
- Composer

Instalación

1) Crear el archivo .env a partir del archivo .env.example, asignar las variables
DB_CONNECTION,DB_HOST,DB_PORT,DB_DATABASE,DB_USERNAME,DB_PASSWORD con la base de datos a utlizar.

2) Ejecutar dentro de la carpeta del proyecto

- composer install --no-scripts
- php artisan key:generate.
- php artisan migrate.
    

Despues de ejecutar la migración se crea la tabla en base de datos para alamacenar los avatars.

3) Configuración

config/image.php
 
- driver: ("gd", "imagick") dependiendo de la libreria instalada, por defecto "gd"
- base_path : directorio para almacenar avatars, por defecto app storage.
    
    
4) Test y ejecución

Para ejecutar los test

    - carpeta_proyecto$ ./vendor/bin/phpunit tests/AvatarTest.php

Para ejecutar los servicios restfull

    - http://{url_proyecto}/public/api/{nombre_recurso}
    - recursos
            -avatars (GET,POST,DELETE)
            -confirmation (GET)