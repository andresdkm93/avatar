<?php
/**
 * Created by PhpStorm.
 * User: andressdkm
 * Date: 7/11/16
 * Time: 03:04 PM
 */

namespace App\Http\Controllers;

use App\Utils\ResponseUtil;
use Illuminate\Support\Facades\Response;

class BaseController extends Controller
{

    public function sendResponse($result)
    {
        return Response::json(ResponseUtil::makeResponse($result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error, $code), $code);
    }
}
