<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Requests\GetAvatarRequest;
use App\Repositories\AvatarRepositoryEloquent;
use App\Http\Requests\CreateAvatarRequest;

class AvatarAPIController extends BaseController
{
    private $avatarRepository;

    public function __construct(AvatarRepositoryEloquent $avatarRepo)
    {
        $this->avatarRepository = $avatarRepo;
    }

    /**
     * Store a newly avatar.
     * POST /avatars
     *
     * @param CreateAvatarRequest $request
     *
     * @return Response
     */
    public function store(CreateAvatarRequest $request)
    {
        $input = $request->all();
        $this->avatarRepository->create($input);
        return response("", 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $emailHash
     * @param GetAvatarRequest $request
     * @return \Illuminate\Http\Response
     */
    public function show($emailHash, GetAvatarRequest $request)
    {
        $d=$request->input('d');
        $s=$request->input('s');
        $response = $this->avatarRepository->get($emailHash, $d, $s, $request->headers->get('Accept'));
        if ($response) {
            return $response;
        }
        return $this->sendError('Avatar not found');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $emailHash
     * @return \Illuminate\Http\Response
     */
    public function destroy($emailHash)
    {
        $avatar = $this->avatarRepository->findByField('email_hash', $emailHash)->first();
        if (empty($avatar)) {
            return $this->sendError('Avatar not found');
        }
        $this->avatarRepository->delete($avatar->email);
        return $this->sendResponse(['email' => $avatar->email]);
    }
}
