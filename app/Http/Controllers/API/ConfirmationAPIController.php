<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Repositories\ConfirmationRepositoryEloquent;

class ConfirmationAPIController extends BaseController
{
    private $confirmationRepository;

    public function __construct(ConfirmationRepositoryEloquent $confirmationRepo)
    {
        $this->confirmationRepository = $confirmationRepo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $code
     * @return \Illuminate\Http\Response
     */
    public function confirm($code)
    {
        if (strlen($code) < 32) {
            $avatar = $this->confirmationRepository->findByField('code_delete', $code)->first();
            if (empty($avatar)) {
                return $this->sendError('Avatar not found');
            }
            $this->confirmationRepository->delete($avatar->id);
        } else {
            $avatar = $this->confirmationRepository->findByField('email_hash', $code)->first();
            if (empty($avatar)) {
                return $this->sendError('Avatar not found');
            }
        }
        return $this->sendResponse(['email' => $avatar->email]);
    }
}
