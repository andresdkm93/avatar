<?php

namespace App\Http\Requests;

use App\Utils\Mimetype;
use App\Utils\APIRequest;

class CreateAvatarRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $mimes=Mimetype::getValues();
        return [
            'email' => 'required|string|email',
            'image'  => 'required|string',
            'mime-type'=> 'required|in:'.implode(",", $mimes)
        ];
    }
}
