<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Utils\APIRequest;

class GetAvatarRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regExpColor="^#(?:[0-9a-f]{3}){1,2}$";
        return [
            "d" => array("regex:/(".$regExpColor.")|(^blank$)|(^https?)|(^404$)/"),
            "s" => "integer"
        ];
    }
}
