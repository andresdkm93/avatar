<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;
use App\Utils\Mimetype;
use App\Utils\ResponseUtil;
use Illuminate\Support\Facades\Response;

class CheckMimeType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $mimes = Mimetype::getValues();
        $header = $request->headers->get('Accept');
        $validator = Validator::make(["Accept" => $header], [
            'Accept' => 'required|in:' . implode(",", $mimes),
        ]);

        if ($validator->fails()) {
            return Response::json(ResponseUtil::makeError($validator->errors()->first('Accept'), 400), 400);
        }
        return $next($request);
    }
}
