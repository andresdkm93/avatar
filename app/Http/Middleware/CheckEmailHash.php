<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use App\Utils\ResponseUtil;

class CheckEmailHash
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id=$request->route('emailHash');
        $validator = Validator::make(["emailHash" => $id], [
            'emailHash' => 'string|size:32',
        ]);

        if ($validator->fails()) {
            return Response::json(ResponseUtil::makeError($validator->errors()->first('emailHash'), 400), 400);
        }
        return $next($request);
    }
}
