<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConfirmationRepository
 * @package namespace App\Repositories;
 */
interface ConfirmationRepository extends RepositoryInterface
{
    //
}
