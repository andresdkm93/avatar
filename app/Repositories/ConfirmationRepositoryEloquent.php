<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Services\ImageService;
use App\Entities\Avatar;

/**
 * Class ConfirmationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ConfirmationRepositoryEloquent extends BaseRepository implements ConfirmationRepository
{
    private $imageService;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Avatar::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->imageService=new ImageService();
    }

    public function delete($id)
    {
        $avatar = parent::delete($id);
        $this->imageService->dropImage($avatar->email_hash);
        return $avatar;
    }
}
