<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AvatarRepository
 * @package namespace App\Repositories;
 */
interface AvatarRepository extends RepositoryInterface
{
    //
}
