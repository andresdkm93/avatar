<?php

namespace App\Repositories;

use Intervention\Image\Facades\Image;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Avatar;
use App\Services\ImageService;
use App\Services\MailService;

/**
 * Class AvatarRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AvatarRepositoryEloquent extends BaseRepository implements AvatarRepository
{
    /**/
    private $imageService;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Avatar::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->imageService = new ImageService();
    }

    /**
     * create avatar
     * @param array $attributes to create avatar
     * @return Avatar
     */
    public function create(array $attributes)
    {
        $avatar = $this->model->where('email', $attributes['email'])->first();
        if (empty($avatar)) {
            $attributes['email_hash'] = md5($attributes['email']);
            $attributes['mime_type'] = $attributes['mime-type'];
            $avatar = parent::create($attributes);
        } else {
            $avatar->mime_type = $attributes['mime-type'];
            $avatar->save();
        }
        $this->imageService->dropImage($avatar->email_hash);
        $this->imageService->saveImage($attributes['image'], $avatar->email_hash, $attributes['mime-type']);
        return $avatar;
    }

    /**
     * get avatar
     * @param string $id
     * @param string $d
     * @param string $s
     * @param string $header
     * @return Image
     */
    public function get($id, $d, $s, $header)
    {
        $avatar = $this->model->where('email_hash', $id)->first();
        if (empty($avatar)) {
            return $this->defaultImage($d, $s, $header);
        }

        if ($this->imageService->imageExists($id, $header)) {
            $image = $this->imageService->getImage($id, $header);
        } else {
            $image = $this->imageService->encodeImage($id, $avatar->mime_type, $header);
        }

        if ($s) {
            $image = $image->resize($s, $s);
        }
        return $image->response($header);
    }


    private function defaultImage($d, $s = 80, $mime = "image/gif")
    {
        if ($s == null) {
            $s = 80;
        }
        switch ($d) {
            case 'blank':
                return $this->imageService->createImage($s, $mime)->response();
                break;
            case '404':
                return null;
                break;
            default:
                if (filter_var($d, FILTER_VALIDATE_URL)) {
                    return $this->imageService->loadImage($d, $s)->response();
                } elseif ($d) {
                    return $this->imageService->createImage($s, $mime, $d)->response();
                } else {
                    return $this->imageService->loadImage(null, $s)->response();
                }
                break;
        }
    }

    /**
     * send email hash to mail
     * @param string $email
     * @return void
     */
    public function delete($email)
    {
        $avatar = Avatar::where('email', $email)->first();
        $avatar->code_delete = $this->generateCodeNumber();
        $avatar->save();
    }

    /**
     * get unique code
     * @return int
     */
    private function generateCodeNumber()
    {
        $number = mt_rand(1000000000, 9999999999);
        if ($this->codeNumberExists($number)) {
            return $this->generateBarcodeNumber();
        }
        return $number;
    }

    /**
     * validate code
     * @param int $number
     * @return Avatar
     */
    private function codeNumberExists($number)
    {
        return $this->model->where('code_delete', $number)->first();
    }
}
