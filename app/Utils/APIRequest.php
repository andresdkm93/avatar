<?php
/**
 * Created by PhpStorm.
 * User: andressdkm
 * Date: 7/11/16
 * Time: 04:11 PM
 */

namespace App\Utils;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

class APIRequest extends FormRequest
{
    /**
     * Get the proper failed validation response for the request.
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $messages = implode(' ', array_flatten($errors));
        return Response::json(ResponseUtil::makeError($messages, 400), 400);
    }
}
