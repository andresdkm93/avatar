<?php
/**
 * Created by PhpStorm.
 * User: andressdkm
 * Date: 7/11/16
 * Time: 03:33 PM
 */

namespace App\Utils;

use WBoyz\LaravelEnum\BaseEnum;

class Mimetype extends BaseEnum
{
    const JPG = "image/jpeg";
    const PNG = "image/png";
    const GIF = "image/gif";
    const BMP = "image/bmp";
}
