<?php
/**
 * Created by PhpStorm.
 * User: andressdkm
 * Date: 7/11/16
 * Time: 03:03 PM
 */

namespace App\Utils;

class ResponseUtil
{
    /**
     * @param string $data
     * @param mixed $data
     *
     * @return array
     */
    public static function makeResponse($data)
    {
        return $data;
    }

    /**
     * @param string $message
     * @param string $code
     * @param array $data
     *
     * @return array
     */
    public static function makeError($message, $code, array $data = [])
    {
        $res = [
            'message' => $message,
            'code' => $code,
            "link" => "http://laravel.com/docs"
        ];

        if (!empty($data)) {
            $res['data'] = $data;
        }

        return $res;
    }
}
