<?php
/**
 * Created by PhpStorm.
 * User: andressdkm
 * Date: 7/11/16
 * Time: 04:21 PM
 */

namespace App\Services;

use Intervention\Image\Facades\Image;

class ImageHandler
{

    private $basePath;

    public function __construct($url)
    {
        $this->basePath = $url;
    }

    /**
     * Check if the image exists
     *
     * @param  string $name
     * @param  string $mime
     * @return boolean
     */
    public function imageExists($name, $mime)
    {
        $extension = $this->getEncode($mime);
        $file = $this->basePath . $name . "." . $extension;
        return file_exists($file) && is_readable($file);
    }

    /**
     * save imge from url-data
     *
     * @param  string $url
     * @param  string $name
     * @param  string $mime
     * @return void
     */
    public function saveImage($url, $name, $mime = "image/png")
    {
        $extension = $this->getEncode($mime);
        $img = Image::make($url)->encode($extension);
        $img->save($this->basePath . $name . "." . $extension);
    }


    /**
     * Remove all avatars from a user
     *
     * @param  string $name is file name
     * @param  string $extension is file name
     * @return void
     */
    public function dropImage($name, $extension = "*")
    {
        array_map("unlink", glob($this->basePath . $name . "." . $extension));
    }

    /**
     * Get avatar
     *
     * @param  string $name extension of header request
     * @param  string $mime default extension avatar
     * @return Image
     */
    public function getImage($name, $mime)
    {
        $extension = $this->getEncode($mime);
        $file = $this->basePath . $name . "." . $extension;
        return Image::make($file);
    }

    /**
     * create canvas image
     * @param  string $size
     * @param  string $mime default extension avatar
     * @param string $color
     * @return Image
     */
    public function createImage($size, $mime = "image/png", $color = "#ffffff")
    {
        $extension = $this->getEncode($mime);
        return Image::canvas($size, $size, $color)->encode($extension);
    }

    /**
     * encode image from other image
     * @param  string $name
     * @param  string $mime default extension avatar
     * @param  string $mimeEncode
     * @return Image
     */
    public function encodeImage($name, $mime, $mimeEncode)
    {
        $extension = $this->getEncode($mime);
        $extensionToEncode = $this->getEncode($mimeEncode);
        $fileBase = $this->basePath . $name . "." . $extension;
        $fileEncode = $this->basePath . $name . "." . $extensionToEncode;
        $img = Image::make($fileBase)->encode($extensionToEncode);
        $img->save($fileEncode);
        return $img;
    }

    /**
     * Load image from url
     * @param  string $url
     * @param  string $size
     * @return Image
     */
    public function loadImage($url, $size)
    {
        if ($url == null) {
            $url = $this->basePath . "avatar.png";
        }
        return Image::make($url)->resize($size, $size);
    }


    /**
     * Get extension
     * @param  string $mimeType
     * @return string
     */
    public function getEncode($mimeType)
    {
        switch ($mimeType) {
            case 'image/jpeg':
                return 'jpg';
                break;
            case 'image/png':
                return 'png';
                break;
            case 'image/gif':
                return 'gif';
                break;
            case 'image/bmp':
                return 'bmp';
                break;
        }
    }
}
