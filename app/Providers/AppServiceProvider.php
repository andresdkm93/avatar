<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Entities\Avatar;
use App\Observers\AvatarObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Avatar::observe(AvatarObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
