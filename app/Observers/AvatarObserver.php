<?php
/**
 * Created by PhpStorm.
 * User: andressdkm
 * Date: 9/11/16
 * Time: 06:00 PM
 */

namespace App\Observers;

use App\Entities\Avatar;
use Illuminate\Support\Facades\Mail;

class AvatarObserver
{


    public function saved(Avatar $avatar)
    {
        if ($avatar->code_delete!=null) {
            Mail::send('mail.delete', ["code" => $avatar->code_delete], function ($message) use ($avatar) {
                $message->from('avatar@app.com', 'Este es tu codigo');
                $message->to($avatar->email)->subject('Tu codigo de verificación!');
            });
        } else {
            Mail::send('mail.message', ["code" => $avatar->email_hash], function ($message) use ($avatar) {
                $message->from('avatar@app.com', 'Este es tu codigo');
                $message->to($avatar->email)->subject('Tu codigo de verificación!');
            });
        }
    }

}