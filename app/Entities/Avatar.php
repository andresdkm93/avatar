<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Avatar extends Model implements Transformable
{
    use TransformableTrait;

    public $table = "avatars";

    protected $fillable = [
        "email",
        "email_hash",
        "mime_type",
        "code"
    ];

    protected $casts = [
        "email" => "string",
        "email_hash" => "string",
        "mime_type" => "string",
        "code" => "string"
    ];
}
