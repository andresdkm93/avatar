<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/avatars','API\AvatarAPIController@store');

Route::get('/avatars/{emailHash}','API\AvatarAPIController@show')->middleware(['mime','email.hash']);

Route::delete('/avatars/{emailHash}','API\AvatarAPIController@destroy')->middleware(['email.hash']);

Route::get('/confirmation/{code}', 'API\ConfirmationAPIController@confirm');