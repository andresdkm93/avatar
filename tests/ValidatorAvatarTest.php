<?php

namespace App\Tests;

use TestCase;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CreateAvatarRequest;
use App\Http\Requests\GetAvatarRequest;
use Faker;

class ValidatorAvatarTest extends TestCase
{
    /**
     * @param $email
     * @param $image
     * @param $mime
     * @dataProvider failDataPostProvider
     */
    public function testFailCreateAvatarRequest($email = null, $image = null, $mime = null)
    {
        $request = new CreateAvatarRequest();
        $validate = Validator::make([
            "email" => $email,
            "image" => $image,
            "mime-type" => $mime
        ], $request->rules());
        $this->assertTrue($validate->fails());
    }

    public function testSuccessCreateAvatarRequest()
    {
        $request = new CreateAvatarRequest();
        $validate = Validator::make([
            "email" => "example@example.com",
            "image" => "example",
            "mime-type" => "image/png"
        ], $request->rules());
        $this->assertFalse($validate->fails());
    }

    /**
     * @param $d
     * @dataProvider successDataGetProvider
     */
    public function testSuccessGetAvatarRequest($d)
    {
        $request = new GetAvatarRequest();
        $validate = Validator::make([
            "d" => $d,
            "s" => 100
        ], $request->rules());
        $this->assertFalse($validate->fails());
    }

    public function testFailGetAvatarRequest()
    {
        $request = new GetAvatarRequest();
        $validate = Validator::make([
            "d" => "example",
        ], $request->rules());
        $this->assertTrue($validate->fails());

        $validate = Validator::make([
            "s" => "example",
        ], $request->rules());
        $this->assertTrue($validate->fails());
    }

    public function failDataPostProvider()
    {
        $faker = Faker\Factory::create();
        $email = $faker->email;
        $image = $faker->imageUrl($width = 640, $height = 480);
        $text = $faker->realText(100);
        $mime = "image/png";
        return [
            'only email' => [$email],
            'only image' => [null, $image],
            'only mime' => [null, null, $mime],
            'email and image' => [$email, $image],
            'image and mime' => [null, $image, $mime],
            'fail email' => [$text, $image, $mime],
            'fail mime' => [$email, $image, $text],
        ];
    }

    public function successDataGetProvider()
    {
        $faker = Faker\Factory::create();
        $image=$faker->imageUrl($width = 640, $height = 480);
        return [
            ["404"],
            ["blank"],
            [$image],
            ["#ff0000"],
        ];
    }
}
