<?php

namespace App\Tests;

use App\Services\ImageHandler;
use TestCase;
use Faker;
use Intervention\Image\File;

class ImageHandlerTest extends TestCase
{


    public function testGetImageFromUrl()
    {
        $faker = Faker\Factory::create();
        $imageHandler = new ImageHandler("./tests/");
        $url = $faker->imageUrl($width = 640, $height = 480);
        $image = $imageHandler->loadImage($url, 80);
        $this->assertInstanceOf(File::class, $image);
    }

    public function testSaveImage()
    {
        $faker = Faker\Factory::create();
        $imageHandler = new ImageHandler("./tests/");
        $name = 'cats';
        $url = $faker->imageUrl(640, 480, $name);
        $imageHandler->saveImage($url, $name);
        $this->assertTrue($imageHandler->imageExists($name, 'image/png'));
    }

    public function testGetImage()
    {
        $imageHandler = new ImageHandler("./tests/");
        $name = 'cats';
        $image = $imageHandler->getImage($name, 'image/png');
        $this->assertInstanceOf(File::class, $image);
    }

    public function testEncodeImage()
    {
        $imageHandler = new ImageHandler("./tests/");
        $name = 'cats';
        $image = $imageHandler->encodeImage($name, 'image/png', 'image/gif');
        $this->assertInstanceOf(File::class, $image);
    }

    public function testCreateImage()
    {
        $faker = Faker\Factory::create();
        $imageHandler = new ImageHandler("./tests/");
        $size = 80;
        $color = $faker->hexColor;
        $image = $imageHandler->createImage($size, 'image/png', $color);
        $this->assertInstanceOf(File::class, $image);
    }

    public function testRemoveImage()
    {
        $imageHandler = new ImageHandler("./tests/");
        $name = 'cats';
        $imageHandler->dropImage($name);
        $this->assertFalse($imageHandler->imageExists($name, 'image/png'));
    }

}
